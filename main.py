from functools import wraps
from time import time


def performance(func):
    @wraps(func)
    def out(*args, **kwargs):
        begin = time()
        result = func(*args, **kwargs)
        end = time()
        print(f'Result is {result}. Calculated in: {round(end - begin, 5)} seconds')
        return result

    return out


@performance
def nwd_euclidean_simple(a, b):
    while (a != b):
        if (a > b):
            a -= b
        else:
            b -= a
        # print(f"Step : a={a}, b={b}")
    return a


# https://mattomatti.com/pl/a0031
@performance
def nwd_euclidean(a, b):
    if (a == b):
        return a
    if (a == 0):
        return b
    if (b == 0):
        return a

    result = 1

    while (a != b):
        if (b > a):
            tmp = a
            a = b
            b = tmp
        if (a % 2 == 0 and b % 2 == 0):
            result *= 2;
            a /= 2;
            b /= 2;
        elif (a % 2 == 0 and b % 2 != 0):
            a /= 2
        elif (a % 2 != 0 and b % 2 == 0):
            b /= 2;
        elif (a % 2 != 0 and b % 2 != 0):
            a = abs(a - b) / 2;

    return int(result * a)


# https://codility.com/media/train/10-Gcd.pdf
def nwd_euclidean_recursive(a, b, res):
    if a == b:
        return res * a
    elif (a % 2 == 0) and (b % 2 == 0):
        return nwd_euclidean_recursive(a // 2, b // 2, 2 * res)
    elif (a % 2 == 0):
        return nwd_euclidean_recursive(a // 2, b, res)
    elif (b % 2 == 0):
        return nwd_euclidean_recursive(a, b // 2, res)
    elif a > b:
        return nwd_euclidean_recursive(a - b, b, res)
    else:
        return nwd_euclidean_recursive(a, b - a, res)


# https://mattomatti.com/pl/a0031
@performance
def nwd_binary_iterative(a, b):
    if (a == b):
        return a;
    if (a == 0):
        return b;
    if (b == 0):
        return a;

    result = 1

    while (a != b):
        if (b > a):
            tmp = a
            a = b
            b = tmp

        x = ((a & 1) << 1) | (b & 1)

        if (x == 0):
            result <<= 1
            a >>= 1
            b >>= 1
        elif (x == 1):
            a >>= 1
        elif (x == 2):
            b >>= 1;
        elif (x == 3):
            a = (abs(a - b) >> 1)

    return result * a


if __name__ == '__main__':
    try:
        a = int(input('a: '))
        b = int(input('b: '))

        print(f"Simple: a={a}, b={b}, result={nwd_euclidean_simple(a, b)}")
        print(f"Binary: a={a}, b={b}, result={nwd_binary_iterative(a, b)}")
    except Exception as e:
        print('Incorrect input: ' + str(e))
        raise e
