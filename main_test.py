import unittest

import main


class TestEuclidian(unittest.TestCase):

    def test_simple_euclidian(self):
        self.assertEqual(main.nwd_euclidean_simple(1989, 867), 51)
        self.assertEqual(main.nwd_euclidean_simple(543543, 343), 7)
        self.assertEqual(main.nwd_euclidean_simple(243454, 324), 2)

    def test_advanced_euclidian(self):
        self.assertEqual(main.nwd_euclidean(1989, 867), 51)
        self.assertEqual(main.nwd_euclidean(543543, 343), 7)
        self.assertEqual(main.nwd_euclidean(243454, 324), 2)

    def test_euclidean_recursive(self):
        self.assertEqual(main.nwd_euclidean_recursive(1989, 867, 1), 51)
        self.assertEqual(main.nwd_euclidean_recursive(543543, 343, 1), 7)
        self.assertEqual(main.nwd_euclidean_recursive(243454, 324, 1), 2)


    def test_binary_iterative(self):
        self.assertEqual(main.nwd_binary_iterative(1989, 867), 51)
        self.assertEqual(main.nwd_binary_iterative(543543, 343), 7)
        self.assertEqual(main.nwd_binary_iterative(243454, 324), 2)


if __name__ == '__main__':
    unittest.main()
